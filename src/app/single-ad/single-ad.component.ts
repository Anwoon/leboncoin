import {Component, DoCheck, OnInit} from '@angular/core';
import {AdService} from '../services/ad.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-single-ad',
  templateUrl: './single-ad.component.html',
  styleUrls: ['./single-ad.component.scss']
})
export class SingleAdComponent implements OnInit{

  id: number;
  ad: any;

  constructor(private adService: AdService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.adService.subject.subscribe((data) => {  this.ad = data.find((ad) =>  ad.id === this.id); });
    this.adService.getAnnounces();
  }
}
