import {Component, OnInit} from '@angular/core';
import {AdService} from '../services/ad.service';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss']
})
export class CounterComponent implements OnInit {
  count: number;
  constructor(private adService: AdService) { }

  ngOnInit() {
    this.adService.subject.subscribe(() => this.count = this.adService.Count() );
  }
}
