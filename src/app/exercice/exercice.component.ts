import {Component, OnInit} from '@angular/core';
import 'rxjs/add/observable/interval';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';

@Component({
  selector: 'app-exercice',
  templateUrl: './exercice.component.html',
  styleUrls: ['./exercice.component.scss']
})
export class ExerciceComponent implements OnInit {

  number: number = 0;
  subscription;
  count: number = 0;
  interval;
  click = false;

  constructor() {
  }

  ngOnInit() {
  }

  counts() {
    if (this.click === false) {
      let observable = Observable.interval(1000);
      this.click = true;
      this.subscription = observable.subscribe(
        () => this.number++,
        () => console.log('error'),
        () => console.log('finish')
      );
    }
  }

  stopCount() {
    this.subscription.unsubscribe();
    this.number = 0;
    this.click = false;
  }

  count2() {
    // if (this.click === false) {
    //   this.click = true;
    let observable = Observable.create((observer) => {
      let interval = setInterval(() => {
        observer.next(this.number++);
        this.stopCount2(interval);
      }, 1000);
    });
    this.subscription = observable.subscribe();
    // }
  }

  stopCount2(interval) {
    if (this.number >= 4) {
      clearInterval(interval);
      this.subscription.unsubscribe();
    }
  }

  count3() {
    let subject = new Subject<number>();

    let interval = setInterval(() => {
      subject.next(this.number++);
      this.stopCount3(interval);
    }, 1000);

    this.subscription = subject.subscribe();
  }

  stopCount3(interval) {
    if (this.number >= 4) {
      clearInterval(interval);
      this.subscription.unsubscribe();
      ;
    }
  }

  stopAleatoire(interval) {
    if (this.count >= 4) {
      clearInterval(interval);
      this.subscription.unsubscribe();
    }
  }

  counterLaunch(){
    var pas: number;
    this.click = false
    let observable = Observable.create((observer) => {
      observer.next( pas = Math.floor(Math.random() * 3))
      observer.next( this.interval = setInterval(() => { this.number = this.number + pas;  this.count++; this.stopa() }, 1000))
      observer.complete();
    })
    this.subscription = observable.subscribe();
  }

  stopa() {
    if (this.count >= 4){
      clearInterval(this.interval);
      this.subscription.unsubscribe();
    }
  }


}
