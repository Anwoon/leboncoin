import {Announce} from '../models/announce';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class AdService {
  private announces: Announce[] = [];
  announce: Announce;
  counter = 0;
  subject: Subject<any>;

  constructor(public http: HttpClient) {
    this.subject = new Subject<any>();
  }

  getEmit() {
    this.subject.next(this.announces);
  }

  pushData(data) {

    const ads = [];
    for (const [id, ad] of Object.entries(data)) {
      ad.id = id
      ad.date = Date.parse(ad.date)
      ads.push(ad);
    }

    this.announces = ads;
  }

  pushAnnounce(data) {
    const ads = [];
    for (const [id, ad] of Object.entries({data})) {
      ads.push(ad);
    }
    this.announce = ads[0];
  }

  getAnnounce(id) {
    this.http.get<any>('https://leboncoin-424da.firebaseio.com/ads/' + id + '.json').subscribe(
      (data) => { this.pushAnnounce(data);
        this.getEmit();
      },
      (error) => console.log(error),
      () => console.log('complete'),
    );
  }

  getAnnounces() {
    this.http.get<any>('https://leboncoin-424da.firebaseio.com/ads.json').subscribe(
      (data) => { this.pushData(data);
        this.getEmit();
       },
      (error) => console.log('error', error),
      () => console.log('complete'),
    );
  }

  SwitchBrouillon(index: number) {
    this.announces[index].status = 'Brouillon';
    this.getEmit();
  }

  SwitchPublier(index: number) {
    this.announces[index].status = 'Publié';
    this.getEmit();
  }

  SwitchDesactiver(index: number) {
    this.announces[index].status = 'Désactivée';
    this.getEmit();
  }

  Count() {
    this.counter = 0;
    this.announces.forEach((announce) => {
      if (announce.status === 'Publié') {
        this.counter++;
      }
    });

    return this.counter;
  }

  addAdd(form) {
    let announce = Announce.create(form.value);
    this.http.post('https://leboncoin-424da.firebaseio.com/ads.json', announce).subscribe(
      (response) => console.log(response),
      (error) => console.log(error),
      () => this.getAnnounces(),
    );
  }

  deleteAdd(id) {
    this.http.delete('https://leboncoin-424da.firebaseio.com/ads/' + id + '.json').subscribe(
      (response) => {
        const index = this.announces.findIndex((element) => element.id === id );
      this.announces.splice(index, 1);
      this.getEmit(); },
      (error) => console.log(error),
      () => console.log('complete')
    );
  }

  getOneArrayWithId(data) {
    const ads = [];
    for (const [id, ad] of Object.entries({data})) {
      ads.push(ad);
    }
    return ads[0];
  }

  updateAdd(form, id) {
    let data = Announce.create(form.value);
    this.http.put('https://leboncoin-424da.firebaseio.com/ads/' + id + '.json', data).subscribe(
      (response) => {
        const index = this.announces.findIndex((element) => element.id === id );
        let data = this.getOneArrayWithId(response);
        this.announces.splice(index, 1, data);
        this.getEmit();
      },
    (error) => console.log(error)
    );
  }

}
