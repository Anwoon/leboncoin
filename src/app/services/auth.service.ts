import {Subject} from 'rxjs/Subject';

export class AuthService{
  isAuth: boolean = false;
  public AuthSubject: Subject<any> = new Subject<any>();

  emitAuth() {
    this.AuthSubject.next(this.isAuth);
  }

  isSignIn() {
    return new Promise(
      (resolve, reject) => {
        setTimeout(
          () => {
            this.isAuth = true;
            this.emitAuth();
            resolve(true);
          }, 1000
        );
      }
    );
  }

  isSignOut() {
    this.isAuth = false;
  }
}
