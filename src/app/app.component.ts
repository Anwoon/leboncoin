import {Component, DoCheck, Input, OnInit} from '@angular/core';
import {Announce} from './models/announce';
import {AdService} from './services/ad.service';
import {AuthService} from './services/auth.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  auth: boolean;
  constructor(private authService: AuthService) {}

  ngOnInit() {
    this.authService.AuthSubject.subscribe(
      (data) => this.auth = data,
      () => console.log('error')
    );
  }

}
