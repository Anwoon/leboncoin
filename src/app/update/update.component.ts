import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AdService} from '../services/ad.service';
import {Announce} from '../models/announce';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss']
})
export class UpdateComponent implements OnInit {

  id: string;
  formUpdate: FormGroup;
  ad: Announce;

  constructor(private adService: AdService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.adService.subject.subscribe((data) => {  this.ad = data.find((ad) =>  ad.id === this.id); });
    this.adService.getAnnounces();

    this.formUpdate = new FormGroup({
      title: new FormControl('', [Validators.required]),
      prix: new FormControl('', [Validators.required]),
      picture: new FormControl('', [Validators.required]),
      status: new FormControl('', [Validators.required]),
      id: new FormControl('')
    });
  }

  onUpdate(form) {

    if (form.value.prix === '') {
      form.value.prix = this.ad.prix;
    }

    if (form.value.title === '') {
      form.value.title = this.ad.title;
    }

    if (form.value.picture === '') {
      form.value.picture = this.ad.picture;
    }

    if (form.value.status === '') {
      form.value.status = this.ad.status;
    }

    if (form.value.id === '') {
      form.value.id = this.ad.id;
    }

    this.adService.updateAdd(form, this.id);
    this.router.navigate(['ads']);
  }

}
