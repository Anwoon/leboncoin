import {Component, Input, OnInit} from '@angular/core';
import {AdService} from '../services/ad.service';

@Component({
  selector: 'app-ad',
  templateUrl: './ad.component.html',
  styleUrls: ['./ad.component.scss']
})
export class AdComponent implements OnInit {
  @Input() annonceName: string;
  @Input() annonceStatus: string;
  @Input() annonceDate: Date;
  @Input() annoncePrix: number;
  @Input() annonceIndex: number;
  @Input() annonceId: string;

  checkColor() {
    switch (this.annonceStatus) {
      case 'Publié': {
        return 'green';
      }
      case 'Désactivée': {
        return 'red';
      }
      case 'Brouillon': {
        return 'grey';
      }
    }
  }

  constructor(private adService: AdService) { }

  ngOnInit() {
  }

  Brouillon() {
    this.adService.SwitchBrouillon(this.annonceIndex);
  }

  Publier() {
    this.adService.SwitchPublier(this.annonceIndex);
  }

  Desactiver() {
    this.adService.SwitchDesactiver(this.annonceIndex);
  }

  Delete(id) {
    this.adService.deleteAdd(id);
  }
}
