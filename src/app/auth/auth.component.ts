import { Component, OnInit } from '@angular/core';
import {AuthService} from '../services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  Auth: boolean;
  constructor(private authService: AuthService, private route: Router) { }

  ngOnInit() {
    this.Auth = this.authService.isAuth;
  }

  Login() {
    this.authService.isSignIn().then(
      () => {
        console.log('Connexion Réussi!');
        this.Auth = this.authService.isAuth;
      }
    ).then(() => this.route.navigate(['']));
  }

  Logout() {
    this.authService.isSignOut();
    this.Auth = this.authService.isAuth;
  }

}
