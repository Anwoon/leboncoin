export class Announce {
  id: string
  title: string;
  status: string;
  date: Date;
  prix: number;
  picture: string;

  constructor(id = '', title = '', status = '', prix= 0, picture= '') {
    this.id = id;
    this.title = title;
    this.status = status;
    this.date = new Date();
    this.prix = prix;
    this.picture = picture;
  }

  static create(o = {}) {
    const announce = new Announce();
    return Object.assign(announce, o);
  }
}
