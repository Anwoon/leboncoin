import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AdService} from '../services/ad.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {
  form: FormGroup;

  constructor(private adService: AdService, private router: Router) { }

  ngOnInit() {
    this.form = new FormGroup({
      title: new FormControl('', [Validators.required]),
      prix: new FormControl('', [Validators.required]),
      picture: new FormControl('', [Validators.required]),
      status: new FormControl('', [Validators.required])
    });
  }

  onSubmit(form) {
    form.date = new Date();
    this.adService.addAdd(form);
    this.router.navigate(['ads']);
  }

}
