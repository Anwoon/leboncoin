import {Component, DoCheck, OnInit} from '@angular/core';
import {Announce} from '../models/announce';
import {AdService} from '../services/ad.service';

@Component({
  selector: 'app-ad-view',
  templateUrl: './ad-view.component.html',
  styleUrls: ['./ad-view.component.scss']
})
export class AdViewComponent implements  OnInit {
  isAuthenticated = true;
  title = 'Mes annonces';
  announces: any = [];
  count: number;

  constructor(private adService: AdService) {

  }

  ngOnInit() {
    this.adService.subject.subscribe((data) => this.announces = data);
    this.adService.getAnnounces();
  }

  Publie() {
    const pub = 'Publié';
    this.announces.forEach(announce => announce.status = pub);
  }

}
