import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';


import { AppComponent } from './app.component';
import { AdComponent } from './ad/ad.component';
import {AdService} from './services/ad.service';
import { CounterComponent } from './counter/counter.component';
import { AdViewComponent } from './ad-view/ad-view.component';
import {Routes, RouterModule} from '@angular/router';
import { AuthComponent } from './auth/auth.component';
import {AuthService} from './services/auth.service';
import { SingleAdComponent } from './single-ad/single-ad.component';
import { ErrorComponent } from './error/error.component';
import {AuthGuard} from './services/auth-guard.service';
import { EditComponent } from './edit/edit.component';
import {HttpClientModule} from '@angular/common/http';
import { ExerciceComponent } from './exercice/exercice.component';
import { UpdateComponent } from './update/update.component';


const appRoutes: Routes = [
  { path: 'ads', canActivate: [AuthGuard], component: AdViewComponent },
  { path: 'auth', component: AuthComponent} ,
  { path: '', canActivate: [AuthGuard], component: AdViewComponent },
  { path: 'ads/:id', canActivate: [AuthGuard], component: SingleAdComponent},
  { path: 'edit/:id', canActivate: [AuthGuard], component: UpdateComponent},
  { path: 'new', canActivate: [AuthGuard], component: EditComponent},
  { path: 'exercice', component: ExerciceComponent},
  { path: '**', component: ErrorComponent}

];

@NgModule({
  declarations: [
    AppComponent,
    AdComponent,
    CounterComponent,
    AdViewComponent,
    AuthComponent,
    SingleAdComponent,
    ErrorComponent,
    EditComponent,
    ExerciceComponent,
    UpdateComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    AdService,
    AuthService,
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
